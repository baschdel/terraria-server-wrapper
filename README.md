# Terraria Supervisor

This is a little hacky python script, the wraps around a terraria server and only starts it when necessary, it also comes with some cheaty perks and some chatty perks.

## What does it do?

Its main purpose is to stop the server a few seconds after the last player logged off and to start it back up again when somebody wants to connect.
If you want to connect to the server while it's shut down you will not get a connection this is because this script accepted the connection, sent an error message to the Terraria client (wich doen't display it for some reason) and started the actual server. If you connect again you will get a connection with the real server.

On the other end of your gaming session, when the last player logs off the server will get shut down about half a minute (default configuration) after the last player left the game.
This time limit also applys directly after the server started, and nobody logged in successfully yet, so wait with yor sip of coffe until after you typed in the password.

It can also relay chat and status messages to a Matterbridge chatbridge and show received messages ingame.

## Cheaty perks

Since I was already monitoring the servers output and feeding it commands I also implemented some commands:

- /save
- /dawn
- /noon
- /dusk
- /midnight
- /time

I don't think I have to explain them.
Since this script was made for a server among friends anyone can use those command by simply typing them into the chat.

If you don't like them the way they are, they are implemented in the main.py file.

All status messages from the server are forwarded to the ingame chat.

## Chatty perks

This wrapper script can be set up to connect to a matterbridge (wich can be connected to all kinds of chatservices) using the matterbridge api module.

Messages that get relayed to the chatbrige include:
* Ingame chat
* Status messages from the wrapper script (including commands and responses)
* Player join/leave messages
* Server start/stop messages

Ingame messages startin with "* " will not get relayed to the chatbridge

You can find the matterbridge source code along with documentation over at [https://github.com/42wim/matterbridge](https://github.com/42wim/matterbridge)

NOTE: during testing I noticed that the matterbridge -> terraria_wrapper connection doesn't always work reliable after the wrapper starts up (when it runs for a while it seems to not have any issues), Let me know if you have the same problem and have a clue why that could happen

## Installation and Setup

Make sure you have a somewhat recent version of python 3 and a working Terraria server.

Then `git clone` this repository to where you have space an will find it again.

You can find an example configuration in `terraria_supervisor_config.example.py`, rename or copy it to terraria_supervisor_config.py and fill in the command you use to start your server and the port your server will listen on once started (it will be used for the dummy server, so that the wrapper knows when somebody wants to connect to start the actual server).

In case the server stops too fast you should increase the autoshoutdown_at_counter.
The server will be stopped between ´autoshoutdown_at_counter*autoshutdown_interval´ and ´(autoshoutdown_at_counter+1)*autoshutdown_interval´ after the last player logged off.

## Running it

After you have configured everything you start it by simply running the main.py using python3 from anywhere
