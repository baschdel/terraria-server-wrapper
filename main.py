from dummy_server import TerrariaDummyServer
import terraria_wrapper
from cronjob import Cronjob
import terraria_supervisor_config as config

server = TerrariaDummyServer(config.terraria_server_port,"No Terraria server over here!")
current_wrapper = None
autoshutdown_counter = 0

global matterbridge
matterbridge = None

def on_matterbridge_message(nickname, message, gateway):
	if gateway == config.matterbridge_chat_gateway:
		if not (current_wrapper is None):
			try:
				current_wrapper.say("* "+nickname+": "+message)
			except Exception: #this gets called from a separate thread and may run into a race condition
				pass

def check_autoshutdown():
	global autoshutdown_counter
	if not current_wrapper is None:
		if len(current_wrapper.players) == 0:
			if autoshutdown_counter >= config.autoshoutdown_at_counter:
				current_wrapper.exit()
				autoshutdown_counter = 0
			else:
				autoshutdown_counter += 1
		else:
			autoshutdown_counter = 0

def on_server_start(server):
	global current_wrapper
	current_wrapper = server

def on_parsed_message(server, mtype, player, arg):
	if (mtype == "message" and arg.endswith("%")):	
		return
	print("->", mtype, player, arg)
	global current_wrapper
	current_wrapper = server
	if mtype == "join":
		send_statusmessage(player+" joined the game.")
	if mtype == "left":
		send_statusmessage(player+" left the game.")
	if mtype == "chat" and config.allow_ingame_cheats:
		cheat(server, arg)
	if mtype == "chat" and config.matterbridge_enabled and config.matterbridge_chat_gateway:
		if (not arg.startswith("* ")) and (not (arg == "Backing up world file" and player == "Server")):
			global matterbridge
			matterbridge.send(player, arg, config.matterbridge_chat_gateway)
	if mtype == "message" and server.started:
		server.say(arg)

def cheat(server, command):
	if command == "/dawn":
		server.command("dawn")
	if command == "/noon" or command == "/day":
		server.command("noon")
	if command == "/midnight" or command == "/night":
		server.command("midnight")
	if command == "/dusk":
		server.command("dusk")
	if command == "/save":
		server.command("save")
	if command == "/time":
		server.command("time")

def send_statusmessage(message):
	if not (matterbridge is None):
		matterbridge.send("Server", message ,config.matterbridge_chat_gateway)

Cronjob(check_autoshutdown,config.autoshutdown_interval,daemon=True)

if config.matterbridge_enabled:
	from matterbridge import Matterbridge
	matterbridge = Matterbridge(config.matterbridge_api_server, on_matterbridge_message, token = config.matterbridge_auth_token)

try:
	while True:
		address = server.listen_once()
		print("Connection request - staring server - from: ", address)
		send_statusmessage("starting up …")
		terraria_wrapper.run_server(config.terraria_server_command, on_parsed_message, on_server_start=on_server_start)
		current_wrapper = None
		send_statusmessage("offline until someone wants to join …")
except:
	raise
